import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/helloWorld'
import login from '@/components/login'
import register from '@/components/register'
import verify from '@/components/verify'
import recordList from '@/components/record/list'

var view = {
  register: '/register',
  verify: '/verify',
  login: '/login',
  welcome: '/record/list',
  record: {
    list: '/record/list'
  }
}

Vue.prototype.$view = view

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: view.login,
      name: 'login',
      component: login
    },
    {
      path: view.register,
      name: 'register',
      component: register
    },
    {
      path: view.verify,
      name: 'verify',
      component: verify
    },
    {
      path: view.record.list,
      name: 'recordList',
      component: recordList
    },
    {
      path: '/hello',
      name: 'helloworld',
      component: HelloWorld
    }
  ]
})


import Vue from 'vue'
import router from '../../router'

axios.defaults.baseURL = 'http://localhost:8000/api/back/';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response) {
      switch (error.response.status) {
        case 401:
          router.replace({
            path: '/verify',
            query: {
              redirect: router.currentRoute.fullPath
            }
          })
          break
        case 403:
          router.replace({
            path: '/login',
            query: {
              redirect: router.currentRoute.fullPath
            }
          })
          break
      }
    }
    return Promise.reject(error.response.data)
  }
)

Vue.prototype.$http = axios

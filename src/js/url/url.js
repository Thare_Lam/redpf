import Vue from 'vue'

Vue.prototype.$url = {
  register: 'user/register',
  verify: 'user/verify',
  login: 'user/login',
  logout: 'user/logout',
  checkLogin: 'user/checkLogin',
  record: {
    list: 'record/list.do',
    add: 'record/add.do'
  }
}

import Vue from 'vue'
import qs from 'qs'

Vue.prototype.$utils = {
  isNullOrUndifine: function (str) {
    return str === undefined || str === null || str === '';
  },
  qssf: function (data) {
    return qs.stringify(data)
  },
  checkLogin: function (that) {
    that.$http.post(that.$url.checkLogin, {}).then(res => {
      if (res.data.status) {
        that.$router.push(that.$view.welcome)
      }
    }).catch(error => {
      console.log(error)
    })
  }
}
